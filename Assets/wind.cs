using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wind : MonoBehaviour
{
    public AK.Wwise.Event windEvent; // ������� Wwise ��� �������� ����������
    public GameObject cameraObject;
    public RainController rain;
    private void Start()
    {
        // �������� ������� Wwise ��� ���������� ����������
        if (windEvent != null)
            windEvent.Post(gameObject);
    }


    private void Update()
{


        float cameraHeightRelativeToGameObject = cameraObject.transform.position.y - gameObject.transform.position.y;

        AkSoundEngine.SetRTPCValue("CameraHeight", cameraHeightRelativeToGameObject, gameObject);

        AkSoundEngine.SetRTPCValue("rain_intencity", rain.CurrentIntensity * 100, gameObject);
    

            }
}

