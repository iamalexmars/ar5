using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fxtrigger : MonoBehaviour
{
    public ParticleSystem explosion;
    public ParticleSystem gunshot;
    public ParticleSystem mineexplosion;
    public GameObject mine;
    public ParticleSystem machinegun;
    public AK.Wwise.Event engine;
    public AK.Wwise.Event shot;
    public AK.Wwise.Event turn;
    public AK.Wwise.Event mineevent;
    public AK.Wwise.Event machinegunevent;

    // Start is called before the first frame update
    void Start()
    {
        explosion.Stop();
        gunshot.Stop();
        mineexplosion.Stop();
        machinegun.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Shot()
    {
        explosion.Play();
        gunshot.Play();
        shot.Post(gameObject);
    }
    void engineStart()
    {
        engine.Post(gameObject);
    }

    void turret()
    {
        turn.Post(gameObject);
    }
    void landmineexplosion()
    {
        mineexplosion.Play();
        mineevent.Post(gameObject);

    }
    void machingunshot()
    {
        machinegun.Play();
        machinegunevent.Post(gameObject);
    }

}
