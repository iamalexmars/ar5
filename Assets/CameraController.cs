using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float moveSpeed = 10f;
    public float rotateSpeed = 70f;
    public float zoomSpeed = 1000f;

    private Vector3 _angles;

    private void Start()
    {
        _angles = transform.eulerAngles;
    }

    private void Update()
    {
        Vector3 moveDirection = new Vector3(
            Input.GetAxis("Horizontal"), // A/D (��� ������� �����/������)
            0,
            Input.GetAxis("Vertical")    // W/S (��� ������� �����/����)
        );

        if (Input.GetKey(KeyCode.Q)) moveDirection.y -= 1; // Q
        if (Input.GetKey(KeyCode.E)) moveDirection.y += 1; // E

        transform.position += transform.TransformDirection(moveDirection) * moveSpeed * Time.deltaTime;

        if (Input.GetMouseButton(1)) // ��� ��� ��������
        {
            _angles.x -= Input.GetAxis("Mouse Y") * rotateSpeed * Time.deltaTime;
            _angles.y += Input.GetAxis("Mouse X") * rotateSpeed * Time.deltaTime;
            transform.eulerAngles = _angles;
        }

        if (Input.mouseScrollDelta.y != 0) // �������� ���� ��� ����
        {
            transform.position += transform.forward * Input.mouseScrollDelta.y * zoomSpeed * Time.deltaTime;
        }
    }
}
