using UnityEngine;

public class RainController : MonoBehaviour
{
    public ParticleSystem rainParticleSystem;
    public AnimationCurve rainIntensityCurve;
    public float CurrentIntensity { get; private set; }


    private void Start()
    {
        if (rainParticleSystem == null)
        {
            rainParticleSystem = GetComponent<ParticleSystem>();
        }

        if (rainParticleSystem == null)
        {
            Debug.LogError("ParticleSystem is not assigned in the inspector and was not found on the GameObject.");
            return;
        }

        var emission = rainParticleSystem.emission;
        emission.rateOverTime = new ParticleSystem.MinMaxCurve(1f);
       
    }

    private void Update()
    {
        if (rainParticleSystem == null)
        {
            return;
        }

        // ����� ������� ����� ����, ����� ��� �� ������ �� ����� ������ ����� (160 ������)
        // ����� �������� "���������" �����, ������� ������ ��������� � ��������� �� 0 �� 160
        float cycleTime = Time.time % 160f;

        float intensity = rainIntensityCurve.Evaluate(cycleTime);

        var emission = rainParticleSystem.emission;
        emission.rateOverTime = new ParticleSystem.MinMaxCurve(intensity * 800);

        CurrentIntensity = intensity;
    }

}
