using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class statehandler : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Tracked()
    {
        AkSoundEngine.SetState("Tracking", "Tracked");
    }

    public void Lost()
    {
        AkSoundEngine.SetState("Tracking", "NotObserved");
    }
}
